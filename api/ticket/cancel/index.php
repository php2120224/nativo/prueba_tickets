<?php
include '../../config/header.php';
include_once '../../config/database.php';
include_once '../../entities/ticket.php';

$db = new Database();
$connection = $db->getConnection();

$ticket = new Ticket($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        if (isset($_GET['id'])) { $stmt = $ticket->filter($_GET['id']); }
        else { $stmt = $ticket->read(); }
        $count = $stmt->rowCount();

        if ($count > 0) {
            $tickets["registros"] = array();
            $tickets["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "ticket_description" => $ticket_description,
                    "user_history_description" => $user_history_description,
                    "project" => $project,
                    "comments" => $comments,
                    "status" => $status
                );
                array_push($tickets["registros"], $u);
            }
            echo json_encode($tickets);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
         $data = json_decode(json_encode($_POST));
        //$data = json_decode(file_get_contents("php://input"));

        $ticket->description = $data->description;
        $ticket->user_story_id = $data->user_story_id;
        $ticket->comments = $data->comments ?? null;
        $ticket->status = 1;

        $status = $ticket->create();
        if ($status == []) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Ticket creado correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear el ticket.",
                "objeto":"'.$status[2].'"';
            echo '}';
        }

        break;

    case "PUT":
        // $data = json_decode(json_encode($_POST));
        $data = json_decode(file_get_contents("php://input"));

        $ticket->id = $data->id;

        if ($ticket->cancel()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Ticket cancelado correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al cancelar el ticket."';
            echo '}';
        }
        break;

        case "DELETE":
            $data = json_decode(json_encode($_POST));

            $ticket->id = $data->id;

            if ($ticket->delete()) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Usuario elimminado correctamente"
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al eliminar el usuario"';
                echo '}';
            }
            break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
