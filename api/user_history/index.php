<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/user_history.php';

$db = new Database();
$connection = $db->getConnection();

$userHistory = new UserHistory($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $stmt = $userHistory->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $userHistories["registros"] = array();
            $userHistories["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "description" => $description,
                    "project_id" => $project_id
                );
                array_push($userHistories["registros"], $u);
            }
            echo json_encode($userHistories);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
         $data = json_decode(json_encode($_POST));
        //$data = json_decode(file_get_contents("php://input"));

        $userHistory->description = $data->description;
        $userHistory->project_id = $data->project_id;

        $status = $userHistory->create();
        if ($status[0] == []) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Historia de usuario creada correctamente.",
                "user_history": '.json_encode($status[1]).'
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear la historia de usuario.",
                "objeto":"'.$status[2].'"';
            echo '}';
        }

        break;

    case "PUT":
        $data = json_decode(json_encode($_POST));

        $userHistory->id = $data->id;
        $userHistory->description = $data->description;
        $userHistory->project_id = $data->project_id;

        if ($userHistory->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Historia de usuario actualizada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar la historia de usuario."';
            echo '}';
        }
        break;

        case "DELETE":
            $data = json_decode(json_encode($_POST));

            $userHistory->id = $data->id;

            if ($userHistory->delete()) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Historia de usuario elimminada correctamente."
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al eliminar la historia de usuario."';
                echo '}';
            }
            break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
