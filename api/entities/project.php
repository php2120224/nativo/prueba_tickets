<?php
session_start();
class Project {

    // database connection and table name
    private $conn;
    private $table_name = "projects";

    // object properties
    public $id;
    public $name;
    public $company_id;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            name,
            company_id
        )
        VALUES
        (
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->company_id);
        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name." WHERE company_id=".$_SESSION['company_id'];

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET 
        name='".$this->username."',
        company_id='".$this->company_id."'

        WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
