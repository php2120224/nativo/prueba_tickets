<?php
class Company {

    // database connection and table name
    private $conn;
    private $table_name = "companies";

    // object properties
    public $id;
    public $name;
    public $nit;
    public $phone;
    public $address;
    public $email;


    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            name,
            nit,
            phone,
            address,
            email
        )
        VALUES
        (
            ?,
            ?,
            ?,
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->nit);
        $stmt->bindParam(3, $this->phone);
        $stmt->bindParam(4, $this->address);
        $stmt->bindParam(5, $this->email);
        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET 
        name='".$this->name."',
        nit='".$this->nit."',
        phone='".$this->phone."',
        address='".$this->address."',
        email='".$this->email."'

        WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function validate() {
        $stmt = $this->conn->prepare("SELECT COUNT(*) total FROM ".$this->table_name." WHERE name='".$this->name."'");
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
