<?php
session_start();
class UserHistory {

    // database connection and table name
    private $conn;
    private $table_name = "user_stories";

    // object properties
    public $id;
    public $description;
    public $project_id;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            description,
            project_id
        )
        VALUES
        (
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->description);
        $stmt->bindParam(2, $this->project_id);
        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return array([], $this->conn->lastInsertId()); }
    }

    //R
    public function read() {
        $query = "SELECT us.id AS id, us.description AS description, p.id AS project_id FROM ".$this->table_name." us ";
        $query .= "JOIN projects p ON us.project_id = p.id ";
        $query .= "WHERE p.company_id=".$_SESSION['company_id'];

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET 
        description='".$this->description."',
        project_id='".$this->project_id."'

        WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
