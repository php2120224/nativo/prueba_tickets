<?php
session_start();
class Ticket {

    // database connection and table name
    private $conn;
    private $table_name = "tickets";

    // object properties
    public $id;
    public $description;
    public $user_story_id;
    public $comments;
    public $status;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            description,
            user_story_id,
            comments,
            status
        )
        VALUES
        (
            ?,
            ?,
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->description);
        $stmt->bindParam(2, $this->user_story_id);
        $stmt->bindParam(3, $this->comments);
        $stmt->bindParam(4, $this->status);
        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT t.id AS id, t.description AS ticket_description, us.description AS user_history_description, p.name AS project, t.comments AS comments, t.status AS status FROM ".$this->table_name." t ";
        $query .= "JOIN user_stories us ON t.user_story_id = us.id ";
        $query .= "JOIN projects p ON us.project_id = p.id ";
        $query .= "WHERE p.company_id=".$_SESSION['company_id'];

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function filter($id) {
        $query = "SELECT t.id AS id, t.description AS ticket_description, us.description AS user_history_description, p.name AS project, t.comments AS comments, t.status AS status FROM ".$this->table_name." t ";
        $query .= "JOIN user_stories us ON t.user_story_id = us.id ";
        $query .= "JOIN projects p ON us.project_id = p.id ";
        $query .= "WHERE p.company_id=".$_SESSION['company_id']." AND t.id=".$id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET 
        description='".$this->description."',
        comments='".$this->comments."',
        status='".$this->status."'

        WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function cancel() {
        $query = "UPDATE ".$this->table_name." SET 
        status=3

        WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
