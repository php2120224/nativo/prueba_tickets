<?php
error_reporting(E_ALL ^ E_NOTICE);
class Database
{
    // specify your own database credentials
    // Desarollo
/*  private $host = 'localhost';
    private $db_name = '';
    private $username = 'developer';
    private $password = 'developer';
    public $conn; */
 
    //localhost
    private $host = 'localhost';
    private $db_name = 'project_management';
    private $username = 'root';
    private $password = '1234';
    public $conn;

    // get the database connection
    public function getConnection()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO('mysql:host='.$this->host.';dbname='.$this->db_name, $this->username, $this->password);
            $this->conn->exec('set names utf8');
        } catch (PDOException $exception) {
            echo 'Connection error: '.$exception->getMessage();
        }

        return $this->conn;
    }
}

