<?php
include '../../config/header.php';
include_once '../../config/database.php';
include_once '../../entities/user.php';
session_start();

$db = new Database();
$connection = $db->getConnection();

$user = new User($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $stmt = $user->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $users["registros"] = array();
            $users["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "username" => $username,
                    "company_id" => $company_id
                );
                array_push($users["registros"], $u);
            }
            echo json_encode($users);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
         $data = json_decode(json_encode($_POST));
        //$data = json_decode(file_get_contents("php://input"));

        $user->username = $data->username;

        $verify = $user->login();

        if ($verify->rowCount()  > 0) {
            $login = $verify->fetch(PDO::FETCH_ASSOC);

            if (password_verify($data->password, $login['password'])) {
                $_SESSION['user_id'] = $login['id'];
                $_SESSION['username'] = $login['username'];
                $_SESSION['company_id'] = $login['company_id'];
                header("Location: ../../../views/");
                exit();
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Usuario o contraseña incorrecta."';
                echo '}';
            }
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Usuario o contraseña incorrecta."';
            echo '}';
        }

        break;

    case "PUT":
        $data = json_decode(json_encode($_POST));

        $user->id = $data->id;
        $user->nombre = $data->nombre;
        $user->cedula = $data->cedula;
        $user->membresia = $data->membresia;

        if ($user->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario actualizado correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar el usuario."';
            echo '}';
        }
        break;

        case "DELETE":
            $data = json_decode(json_encode($_POST));

            $user->id = $data->id;

            if ($user->delete()) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Usuario elimminado correctamente"
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al eliminar el usuario"';
                echo '}';
            }
            break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
