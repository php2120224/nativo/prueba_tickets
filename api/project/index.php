<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/project.php';

$db = new Database();
$connection = $db->getConnection();

$project = new Project($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $stmt = $project->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $projects["registros"] = array();
            $projects["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "name" => $name,
                    "company_id" => $company_id
                );
                array_push($projects["registros"], $u);
            }
            echo json_encode($projects);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
         $data = json_decode(json_encode($_POST));
        //$data = json_decode(file_get_contents("php://input"));

        $project->name = $data->name;
        $project->company_id = $data->company_id;

        $status = $project->create();
        if ($status == []) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario creado correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear el usuario.",
                "objeto":"'.$status[2].'"';
            echo '}';
        }

        break;

    case "PUT":
        $data = json_decode(json_encode($_POST));

        $project->id = $data->id;
        $project->nombre = $data->nombre;
        $project->membresia = $data->membresia;

        if ($project->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario actualizado correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar el usuario."';
            echo '}';
        }
        break;

        case "DELETE":
            $data = json_decode(json_encode($_POST));

            $project->id = $data->id;

            if ($project->delete()) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Usuario elimminado correctamente"
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al eliminar el usuario"';
                echo '}';
            }
            break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
