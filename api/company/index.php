<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/company.php';

$db = new Database();
$connection = $db->getConnection();

$company = new Company($connection);

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        $stmt = $company->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $companies["registros"] = array();
            $companies["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "name" => $name,
                    "nit" => $nit,
                    "phone" => $phone,
                    "address" => $address,
                    "email" => $email
                );
                array_push($companies["registros"], $u);
            }
            echo json_encode($companies);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
         $data = json_decode(json_encode($_POST));
        //$data = json_decode(file_get_contents("php://input"));

        $company->name = $data->name;
        $company->nit = $data->nit;
        $company->phone = $data->phone;
        $company->address = $data->address;
        $company->email = $data->email;

        $verify = $company->validate();

        if ($verify["total"] == 0) {
            $status = $company->create();
            if ($status == []) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Compañia creada correctamente."
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al crear la compañia.",
                    "objeto":"'.$status[2].'"';
                echo '}';
            }
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear la compañia '.$company->name.' porque ya existe.",
                "objeto":"'.$status[2].'"';
            echo '}';
        }

        break;

    case "PUT":
        $data = json_decode(json_encode($_POST));

        $company->name = $data->name;
        $company->nit = $data->nit;
        $company->phone = $data->phone;
        $company->address = $data->address;
        $company->email = $data->email;

        if ($company->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Compañia actualizada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar la compañia."';
            echo '}';
        }
        break;

        case "DELETE":
            $data = json_decode(json_encode($_POST));

            $company->id = $data->id;

            if ($company->delete()) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Compañia elimminada correctamente."
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al eliminar la compañia."';
                echo '}';
            }
            break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
