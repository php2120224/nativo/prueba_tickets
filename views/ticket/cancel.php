<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../user/login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es" class="h-100">
<?php include '../base/header.php'; ?>

<body class="d-flex flex-column h-100">
    <!-- Begin page content -->
    <main class="flex-shrink-0">
        <div class="container">
            <h3 class="my-3">Cancelar ticket</h3>

            <form id="myForm" class="row g-3" method="post" autocomplete="off">
                <input type="hidden" name="id" id="id">

                <div class="col-md-4">
                    <label for="description" class="form-label">Descripción</label>
                    <input type="text" class="form-control" id="description" name="description" required autofocus>
                </div>

                <div class="col-md-8">
                    <label for="status" class="form-label">Estado</label>
                    <select class="form-select" id="status" name="status" required>
                    </select>
                </div>

                <div class="col-md-4">
                    <label for="comments" class="form-label">Comentario</label>
                    <input type="text" class="form-control" id="comments" name="comments" required>
                </div>

                <div class="col-12">
                    <a href="index.html" class="btn btn-secondary">Regresar</a>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>

            </form>

        </div>
    </main>

    <?php include '../base/footer.php'; ?>
    <script src="../../js/ticket/edit.js"></script>
</body>
</html>
