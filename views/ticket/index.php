<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../user/login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es" class="h-100">
<?php include '../base/header.php'; ?>

<body class="d-flex flex-column h-100">
    <!-- Begin page content -->
    <main class="flex-shrink-0">
        <div class="container">
            <div class="d-flex justify-content-between">
                <h3 class="my-3">Tickets</h3>
                <div class="d-flex">
                    <a href="./add.php" class="align-self-center btn btn-success">Agregar</a>
                </div>
            </div>

            <table class="table table-hover table-bordered my-3" aria-describedby="titulo">
                <thead class="table-dark">
                    <tr>
                        <th scope="col">Descripción</th>
                        <th scope="col">Historia de usuario</th>
                        <th scope="col">Proyecto</th>
                        <th scope="col">Comentario</th>
                        <th scope="col">Estado</th>
                        <th scope="col"></th>
                    </tr>
                </thead>

                <tbody id="tableBody">
                    <tr>
                        <td>12345</td>
                        <td>JUAN PEREZ</td>
                        <td>0123456789</td>
                        <td>JUANPEREZ@DOMINIO.COM</td>
                        <td>RECURSOS HUMANOS</td>
                        <td>
                            <a href="edit.html" class="btn btn-warning btn-sm me-2">Editar</a>

                            <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal"
                                data-bs-target="#eliminaModal" data-bs-id="1">Eliminar</button>
                        </td>
                    </tr>

                </tbody>
            </table>

        </div>
    </main>

    <div class="modal fade" id="eliminaModal" tabindex="-1" role="dialog" aria-labelledby="eliminaModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="eliminaModalLabel">Aviso</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>¿Desea cancelar este ticket?</p>
                </div>
                <div id="modalBodyUpdatePlaza" class="modal-body"></div>
                <div class="modal-footer">
                    <form id="formCancel" method="POST">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success">Si</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php include '../base/footer.php'; ?>
    <script src="../../js/ticket/index.js"></script>
</body>
</html>
