<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../user/login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es" class="h-100">
<?php include '../base/header.php'; ?>

<body class="d-flex flex-column h-100">
    <!-- Begin page content -->
    <main class="flex-shrink-0">
        <div class="container">
            <h3 class="my-3">Crear ticket</h3>

            <form id="myForm" class="row g-3" method="post" autocomplete="off">

                <div class="col-md-4">
                    <label for="description" class="form-label">Descripción</label>
                    <input type="text" class="form-control" id="description" name="description" required autofocus>
                </div>

                <div class="col-md-8">
                    <label for="user_story_id" class="form-label">Historia de usuario</label>
                    <select class="form-select" id="user_story_id" name="user_story_id" required>
                    </select>
                </div>

                <div class="col-12">
                    <a href="index.html" class="btn btn-secondary">Regresar</a>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>

            </form>

        </div>
    </main>

    <?php include '../base/footer.php'; ?>
    <script src="../../js/ticket/add.js"></script>
</body>
</html>
