<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../user/login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es" class="h-100">
<?php include '../base/header.php'; ?>

<body class="d-flex flex-column h-100">
    <!-- Begin page content -->
    <main class="flex-shrink-0">
        <div class="container">
            <h3 class="my-3">Crear historia de usuario</h3>

            <form id="myForm" class="row g-3" method="post" autocomplete="off">

                <div class="col-md-4">
                    <label for="description" class="form-label">Descripción</label>
                    <input type="text" class="form-control" id="description" name="description" required autofocus>
                </div>

                <div class="col-md-8">
                    <label for="project_id" class="form-label">Proyecto</label>
                    <select class="form-select" id="project_id" name="project_id" required>
                    </select>
                </div>

                <div class="col-md-4">
                    <label for="ticket_description" class="form-label">Descripción ticket</label>
                    <input type="text" class="form-control" id="ticket_description" name="ticket_description" required>
                </div>

                <div class="col-12">
                    <a href="index.html" class="btn btn-secondary">Regresar</a>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>

            </form>

        </div>
    </main>

    <?php include '../base/footer.php'; ?>
    <script src="../../js/user_history/add.js"></script>
</body>
</html>
