<?php
session_start();

if (isset($_SESSION['user_id'])) {
    header("Location: ../home/index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es" class="h-100">
<?php include '../base/header.php'; ?>

<body class="d-flex flex-column h-100">
    <!-- Begin page content -->
    <main class="flex-shrink-0">
        <div class="container">
            <h3 class="my-3">Registrar Usuario</h3>

            <form id="myForm" class="row g-3" method="post" autocomplete="off">

                <div class="col-md-4">
                    <label for="username" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="username" name="username" required autofocus>
                </div>

                <div class="col-md-8">
                    <label for="password" class="form-label">Contraseña</label>
                    <input type="text" class="form-control" id="password" name="password" required>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
                </div>

            </form>

        </div>
    </main>

    <?php include '../base/footer.php'; ?>
    <script src="../../js/user/login.js"></script>
</body>
</html>
