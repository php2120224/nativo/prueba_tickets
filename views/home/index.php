<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../user/login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es" class="h-100">
<?php include '../base/header.php'; ?>

<body class="d-flex flex-column h-100">
    <!-- Begin page content -->
    <main class="flex-shrink-0">
        <div class="container">
            <h3 class="my-3">Binvenido, <?php echo $_SESSION['username']; ?></h3>

        </div>
    </main>

    <?php include '../base/footer.php'; ?>
</body>
</html>
