$(document).ready(function() {
    function fetchData() {
        $.ajax({
            url: localServer + "project/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#project_id").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#project_id").append(`<option value="${res.id}">${res.name}</option>`);
                    });
                  } else { alert(`No hay projectos actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                description: $('#description').val(),
                project_id: $('#project_id').val()
            };

            $.ajax({
                url: localServer + "user_history/",
                method: 'POST',
                data: formData,
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) {
                        var formDataTicket = {
                            description: $('#ticket_description').val(),
                            user_story_id: resp.user_history
                        };

                        $.ajax({
                            url: localServer + "ticket/",
                            method: 'POST',
                            data: formDataTicket,
                            success: function(resp) {
                                // alert(resp.mensaje);
                                if (parseInt(resp.estado)) { location.href = "../../views/user/login.php"; }
                            },
                            error: function(error) { console.error('Error submitting form:', error); }
                        });
                    }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
    fetchData();
});
