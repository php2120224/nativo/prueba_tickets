$(document).ready(function() {
    function fetchData() {
        $.ajax({
            url: localServer + "company/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#company_id").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#company_id").append(`<option value="${res.id}">${res.name}</option>`);
                    });
                  } else { alert(`No hay compañias actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                username: $('#username').val(),
                password: $('#password').val(),
                company_id: $('#company_id').val()
            };

            $.ajax({
                url: localServer + "user/",
                method: 'POST',
                data: formData,
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) { location.href = "../../views/user/login.html"; }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
    fetchData();
});
