$(document).ready(function() {
    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                username: $('#username').val(),
                password: $('#password').val()
            };

            $.ajax({
                url: localServer + "user/auth/",
                method: 'POST',
                data: formData,
                success: function(resp) {

                    if (parseInt(resp.estado)) { location.href = "../../views/index.php"; }
                    else { alert(resp.mensaje); }

                    window.location.reload();
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
});
