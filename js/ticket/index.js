$(document).ready(function() {
    var id;
    function fetchData() {
        $.ajax({
            url: localServer + "ticket/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#tableBody").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#tableBody").append(`
                        <tr>
                            <td>${res.ticket_description}</td>
                            <td>${res.user_history_description}</td>
                            <td>${res.project}</td>
                            <td>${res.comments}</td>
                            <td>${res.status}</td>
                            <td class="d-flex justify-content-around">
                                <a class="btn btn-primary btn-sm me-2" href="./edit.php?id=${res.id}">Editar</a>
                                <a class="open-modal btn btn-danger btn-sm me-2" data-id="${res.id}">Cancelar</a>
                            </td>
                        <tr>
                      `);
                    });
                  } else { alert(`No hay tickets actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    fetchData();

    $(document).on('click', '.open-modal', function() {
        id = $(this).data('id');
        $('#eliminaModal').modal('show');
    });

    function handleCancel(event) {
        event.preventDefault();

        if (document.getElementById('formCancel').checkValidity()) {
            var formData = { id };

            $.ajax({
                url: localServer + "ticket/cancel/",
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(formData),
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) { location.reload(); }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#formCancel').on('submit', handleCancel);
});
