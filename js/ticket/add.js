$(document).ready(function() {
    function fetchData() {
        $.ajax({
            url: localServer + "user_history/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#user_story_id").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#user_story_id").append(`<option value="${res.id}">${res.description}</option>`);
                    });
                  } else { alert(`No hay historias de usuario actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                description: $('#description').val(),
                user_story_id: $('#user_story_id').val()
            };

            $.ajax({
                url: localServer + "ticket/",
                method: 'POST',
                data: formData,
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) {if (parseInt(resp.estado)) { location.href = "./index.php"; }
                }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
    fetchData();
});
