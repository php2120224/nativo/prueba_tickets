$(document).ready(function() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');

    if (!id) {
        alert('No se ha proporcionado un ID de ticket.');
        return;
    }

    function fetchData() {
        $.ajax({
            url: localServer + "ticket/?id=" + id,
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#status").empty();
                if (resp.total > 0) {
                    $("#id").val(resp.registros[0].id);
                    $("#description").val(resp.registros[0].ticket_description);
                    $("#comments").val(resp.registros[0].comments);
                    $("#status").append(`<option value="1" ${resp.registros[0].status == "Activo" ? "selected" : ""}>Activo</option>`);
                    $("#status").append(`<option value="2" ${resp.registros[0].status == "En Proceso" ? "selected" : ""}>En Proceso</option>`);
                    $("#status").append(`<option value="3" ${resp.registros[0].status == "Finalizado" ? "selected" : ""}>Finalizado</option>`);
                  } else { alert(`No hay historias de usuario actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                id: $('#id').val(),
                description: $('#description').val(),
                comments: $('#comments').val(),
                status: $('#status').val()
            };

            $.ajax({
                url: localServer + "ticket/",
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(formData),
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) {if (parseInt(resp.estado)) { location.href = "./index.php"; }
                }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
    fetchData();
});
